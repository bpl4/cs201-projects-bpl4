#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

// Brian Le
// The main purpose of this program, is to convert hex to float

void float_func(char * str, int frac, int exp, int special_b, int add_tofrac); // hex to float conversion
int representation(char * str, int frac, int exp); // for finding the representation

#define BUFFER 4096
int main(int argc, char *argv[])
{
	FILE *ifile = stdin;
	char * file_name = NULL;
	char user[BUFFER];
	int opt = 0;
	int fracbits = 23; // set to the default 32 bit float
	int expbits = 8; // set to default as well.
	int bias = 0;
	int frac_adder = 0;


	while ((opt = getopt(argc, argv, "i:dhbme:E:f:F:vH")) != -1)
	{
		switch (opt)
		{
			case 'i':
				file_name = optarg;
				ifile = fopen(file_name, "r");
				if (ifile == NULL)
				{
					perror("Failed to open the file\n");
					exit(EXIT_FAILURE);
				}
				break;
			case 'b':
				fracbits = 7;
				expbits = 8;
				break;
			case 'd': // needs fixing

				fracbits = 52;
				expbits = 11;
				break;
			case 'h':
				fracbits = 10;
				expbits = 5;
				break;
			case 'm': // bias is -2
				fracbits = 3;
				expbits = 4;
				bias = -2;
				break;
			case 'H':
				printf("Usage: ./hex-2-float [OPTION ...]\n");
				printf("\t-i <file_name> specify the name of the input file (defaults to stdin)\n");
				printf("\t   Settings default to single precision (float, 32-bits, 1-8-23)\n");
				printf("\t-d   use settings for double precision (double, 64-bits, 1-11-52)\n");
				printf("\t-h   use settings for half precision (_Float16, 16-bits, 1-5-10\n");
				printf("\t-b   use settings for half precision (bfloat16, 16-bits, 1-8-7)\n");
				printf("\t-m   use settings for quarter precision (minifloat, 8-bits, 1-4-3  bias -2)\n");
				printf("\t-e # set the number of bits to use for the exponent\n");
				printf("\t-E # set the value used for the exponent bias\n");
				printf("\t-f # set the number of bits to use for the fraction\n");
				printf("\t-F # set the value to add to the fraction (unstored fraction bits)\n");
				printf("\t-v   display the settings (to stderr) before reading input\n");
				printf("\t-H   display this help message and exit\n");
				return EXIT_SUCCESS;
				break;
			case 'e':
				expbits = strtol(optarg, NULL, 10);
				break;
			case 'E':
				bias = strtol(optarg, NULL, 10);
				break;
			case 'f':
				fracbits = strtol(optarg, NULL, 10);
				break;
			case 'F':
				frac_adder = strtol(optarg, NULL, 10);
				break;
			case 'v':
				if (file_name == NULL)
					fprintf(stderr, "\tinput file\t: stdin\n");
				else
					fprintf(stderr, "\tinput file\t: %s\n", file_name);

				fprintf(stderr, "\tnumber of bits\t: %d\n", (fracbits + expbits + 1)); 
				fprintf(stderr, "\tfraction bits\t: %d\n", fracbits);
				if (frac_adder == 0)
					fprintf(stderr, "\tfraction add\t: 1\n");
				if (frac_adder != 0)
					fprintf(stderr, "\tfraction add\t: %d\n", frac_adder);


				fprintf(stderr, "\texponent bits\t: %d\n", expbits);

				if (bias == 0)
					bias = (pow(2, expbits) / 2) - 1;

				fprintf(stderr, "\texponent bias\t: %d\n", bias);
				fprintf(stderr, "\tverbose\t: yes\n");
				break;

			default:
				break;

		}
	}

	while (fgets(user, BUFFER, ifile) != NULL)
	{
		float_func(user, fracbits, expbits, bias, frac_adder);
	}


	if (file_name != NULL)
	{
		fclose(ifile);
	}

	
	return EXIT_SUCCESS;
}

int representation(char * str, int frac, int exp)
{
	unsigned long pmask = 0x1L << (frac + exp);
	char hexstr[BUFFER];
	unsigned long hexnum = 0L;
	int rep_type = 0;
	int count = 0;

	printf("%s", str);
	sscanf(str, "%s", hexstr); // only read read in the first string.
	sscanf(hexstr, "%lx", &hexnum);


	for (int i = exp; i > 0; i--) // exp
	{
		pmask >>= 1L;
		printf("%d", (hexnum & pmask) ? 1 : 0);

		if (hexnum & pmask)
		{
			rep_type = 1;
			count++;
		}

		if (count == exp)
		{
			rep_type = 2;
		}
	}

	return rep_type;
}

void float_func(char * str, int frac, int exp, int special_b, int add_tofrac)
{
	int bias = 0;
	unsigned long pmask = 0x1L << (frac + exp);
	char hexstr[BUFFER];
	unsigned long hexnum = 0L; // this should be in dec
	//char * end = NULL;
	int count = 0; // counter for numbers of 1 in the exp.
	int frac_count = 0; // increments when there is a 1 bit.
	int signbit = 0;
	int rep_type = 0; // 0 is denorm, 1 is norm, 2 is special
	int b_exp = 0; //biased exp.
	int exp_arr[exp];
	int frac_arr[frac];
	double frac_num = 0; // basically our Mantissa.
	double V = 0.0; // floating point value
	int j = 0;
	int frac_add = add_tofrac;
	
	if (special_b != 0) // any special cases where the bias is not calculated the normal way
		bias = special_b;
	else
		bias = (pow(2, exp)/2) - 1;


	printf("%s", str);
	sscanf(str, "%s", hexstr); // only read read in the first string.
	sscanf(hexstr, "%lx", &hexnum);
	//hexnum = strtol(hexstr, &end, 16); // this does not work
	//printf("%lu\n", hexnum);

	// MAY NEED TO DO BY HAND TO FIX THE DOUBLE PROBLEM

	// Printing out the floating point formatting.
	printf("\t%d ", (hexnum & pmask) ? 1 : 0);
	if (hexnum & pmask)
		signbit = 1;


	for (int i = exp; i > 0; i--) // exp
	{
		pmask >>= 1L;
		printf("%d", (hexnum & pmask) ? 1 : 0);
		if (hexnum & pmask)
			exp_arr[i - 1] = 1; // yes, this is storing it backwards...
		if (!(hexnum & pmask))
			exp_arr[i - 1] = 0;

		// checking for representation type.
		if (hexnum & pmask)
		{
			rep_type = 1;
			count++;
		}

		if (count == exp)
		{
			rep_type = 2;
		}
	}

	printf(" ");
	for (int i = frac; i > 0; i--) // frac
	{
		pmask >>= 1L;
		printf("%d", (hexnum & pmask) ? 1 : 0);
		if (hexnum & pmask)
			frac_arr[i - 1] = 1;
		if (!(hexnum & pmask))
			frac_arr[i - 1] = 0;

		if (hexnum & pmask)
			frac_count++;
	}
	printf("\n");

	// Printing out the info.
	printf("\ts ");

	for (int i = exp; i > 0; i--)
	{
		printf("e");
	}
	printf(" ");

	for (int i = frac; i > 0; i--)
	{
		printf("f");
	}
	printf("\n\t");

	// Type of representation (normalized, denorm, or special)
	if (rep_type == 0)
		printf("denormalized value\n\t");
	if (rep_type == 2)
	{
		if (frac_count == 0 && signbit == 1)
			printf("negative infinity\n\n");

		else if (frac_count == 0 && signbit == 0)
			printf("positive infinity\n\n");

		else
			printf("NaN\n\n");

		return;
	}


	
	// Sign
	if (signbit == 1)
		printf("sign:\t\tnegative\n");
	if (signbit == 0)
		printf("sign:\t\tpositive\n");

	// Exponent
	if (rep_type == 0)
	{
		b_exp = 1 - bias;
		printf("\texponent:\t%-10d\n", b_exp);
	}

	if (rep_type == 1)
	{
		for (int i = exp; i > 0; i--) // finding the value of the exp. 
		{
			//printf("%d", exp_arr[i - 1]);
			b_exp += pow(2, (i-1)) * exp_arr[i - 1];
		}
		b_exp = b_exp - bias;
		printf("\texponent:\t%-10d\n", b_exp);
	}

	// Fraction
	j = 1;
	for (int i = frac; i > 0; i--)
	{
		frac_num = (frac_num) + ((1/pow(2, j)) * frac_arr[i-1]);
		j++;
	}

	// Always add 1 to the frac if normalized.
	if (rep_type == 1)
	{
		if (frac_add != 0)
			frac_num += frac_add;
		else
			frac_num += 1;
	}

	printf("\tfraction:\t%.20f\n", frac_num); // FIX THIS 


	// Value in double
	V = pow(-1, signbit) * frac_num * pow(2, b_exp);
	printf("\tvalue:\t\t%.20f\n", V);

	// Value in scientific notation
	printf("\tvalue:\t\t%.20le\n", V);



	printf("\n");


	return;
}

