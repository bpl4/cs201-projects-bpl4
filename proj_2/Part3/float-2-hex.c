#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// Brian Le
// The purpose of this program is to convert a float or double into hex inputted in the terminal when running the program

#define BUFFER 1024
int main(int argc, char *argv[])
{
	FILE *ifile = stdin;
	char user[BUFFER] = {0};
	float fnum = 0.0f;
	double dnum = 0.0;
	char * ptr; // this ptr holds the char and prevents the rest from being read.
	char * buf_ptr = NULL;
	int opt = 0;
	unsigned int unum = 0;
	long unsigned int ul = 0;

	opt = getopt(argc, argv, "fdH");
	if (opt != 'f' && opt != 'd' && opt != 'H' && opt != -1)
	{
		exit(EXIT_FAILURE);
	}

	if (opt == 'H')
	{
		printf("Usage: ./float-2-hex [OPTION ...]\n");
		printf("\t-f   convert the input into floats for hex output (this is the default)\n");
		printf("\t-d   convert the input into doubles for hex output\n");
		printf("\t-H   display this help message and exit\n");
		return EXIT_SUCCESS;
	}

	buf_ptr = fgets(user, BUFFER, ifile);
	while(buf_ptr != NULL) // Out options: -f for float (use by default), -d decimal, -H for help,
	{
		if (opt == 'f' || opt == -1)
		{
			fnum = strtof(user, &ptr);	
			user[strlen(user) -1] = '\0';
			unum = *(unsigned int*) &fnum;
			printf("%-40s\t%.10e\t%.10f\t0x%08x\n", user, fnum, fnum, unum);
			buf_ptr = fgets(user, BUFFER, ifile);
		}

		if (opt == 'd')
		{
			dnum = strtod(user, &ptr);	
			user[strlen(user) -1] = '\0';
			ul = *(long unsigned int*) &dnum;
			printf("%-40s\t%.16le\t%.16lf\t0x%016lx\n", user, dnum, dnum, ul);
			buf_ptr = fgets(user, BUFFER, ifile);
		}
	}
	
	return EXIT_SUCCESS;
}

