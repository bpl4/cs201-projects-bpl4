#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

// Brian Le
// The main purpose of this program, is to convert hex to float

void float_func(char * str, int frac, int exp);

#define BUFFER 4096
int main(int argc, char *argv[])
{
	FILE *ifile = stdin;
	char * file_name = NULL;
	char user[BUFFER];
	int opt = 0;
	int fracbits = 23; // set to the default 32 bit float
	int expbits = 8; // set to default as well.


	while ((opt = getopt(argc, argv, "i:dhbme:E:f:F:vH")) != -1)
	{
		switch (opt)
		{
			case 'i':
				file_name = optarg;
				ifile = fopen(file_name, "r");
				if (ifile == NULL)
				{
					perror("Failed to open the file\n");
					exit(EXIT_FAILURE);
				}
				break;
			case 'b':
				fracbits = 7;
				expbits = 8;
				break;
			case 'd': // needs fixing
				fracbits = 52;
				expbits = 11;
				break;
			case 'h':
				fracbits = 10;
				expbits = 5;
				break;
			case 'm': // bias is -2
				fracbits = 3;
				expbits = 4;
			default:
				break;

		}
	}

	while (fgets(user, BUFFER, ifile) != NULL)
	{
		float_func(user, fracbits, expbits);
	}


	if (file_name != NULL)
	{
		fclose(ifile);
	}

	
	return EXIT_SUCCESS;
}

void float_func(char * str, int frac, int exp)
{
	int bias = (pow(2, exp)/2) - 1;
	unsigned int pmask = 0x1 << (frac + exp);
	char hexstr[BUFFER];
	unsigned long hexnum = 0; // this should be in dec
	char * end = NULL;
	int count = 0; // counter for numbers of 1 in the exp.
	int frac_count = 0; // increments when there is a 1 bit.
	int signbit = 0;
	int rep_type = 0; // 0 is denorm, 1 is norm, 2 is special
	int b_exp = 0; //biased exp.
	int exp_arr[exp];
	int frac_arr[frac];
	double frac_num = 0; // basically our Mantissa.
	double V = 0.0; // floating point value
	

	printf("%s", str);
	sscanf(str, "%s", hexstr); // only read read in the first string.
	//sscanf(hexstr, "%lx", &hexnum);
	hexnum = strtol(hexstr, &end, 16); // converts the hex in string into a int that we can use.
	printf("%lu\n", hexnum);

	// MAY NEED TO DO BY HAND TO FIX THE DOUBLE PROBLEM

	// Printing out the floating point formatting.
	printf("\t%d ", (hexnum & pmask) ? 1 : 0);
	if (hexnum & pmask)
		signbit = 1;


	for (int i = exp; i > 0; i--) // exp
	{
		pmask >>= 1;
		printf("%d", (hexnum & pmask) ? 1 : 0);
		if (hexnum & pmask)
			exp_arr[i - 1] = 1; // yes, this is storing it backwards...
		if (!(hexnum & pmask))
			exp_arr[i - 1] = 0;

		// checking for representation type.
		if (hexnum & pmask)
		{
			rep_type = 1;
			count++;
		}

		if (count == exp)
		{
			rep_type = 2;
		}
	}

	printf(" ");
	for (int i = frac; i > 0; i--) // frac
	{
		pmask >>= 1;
		printf("%d", (hexnum & pmask) ? 1 : 0);
		if (hexnum & pmask)
			frac_arr[i - 1] = 1;
		if (!(hexnum & pmask))
			frac_arr[i - 1] = 0;

		if (hexnum & pmask)
			frac_count++;
	}
	printf("\n");

	// Printing out the info.
	printf("\ts ");

	for (int i = exp; i > 0; i--)
	{
		printf("e");
	}
	printf(" ");

	for (int i = frac; i > 0; i--)
	{
		printf("f");
	}
	printf("\n\t");

	// Type of representation (normalized, denorm, or special)
	if (rep_type == 0)
		printf("denormalized value\n\t");
	if (rep_type == 2)
	{
		if (frac_count == 0 && signbit == 1)
			printf("negative infinity\n\n");

		else if (frac_count == 0 && signbit == 0)
			printf("positive infinity\n\n");

		else
			printf("NaN\n\n");

		return;
	}


	
	// Sign
	if (signbit == 1)
		printf("sign:\t\tnegative\n");
	if (signbit == 0)
		printf("sign:\t\tpositive\n");

	// Exponent
	if (rep_type == 0)
	{
		b_exp = 1 - bias;
		printf("\texponent:\t%d\n", b_exp);
	}

	if (rep_type == 1)
	{
		for (int i = exp; i > 0; i--) // finding the value of the exp. 
		{
			//printf("%d", exp_arr[i - 1]);
			b_exp += pow(2, (i-1)) * exp_arr[i - 1];
		}
		b_exp = b_exp - bias;
		printf("\texponent:\t%d\n", b_exp);
	}

	// Fraction
	int j = 1;
	for (int i = frac; i > 0; i--)
	{
		frac_num = (frac_num) + ((1/pow(2, j)) * frac_arr[i-1]);
		j++;
	}

	// Always add 1 to the frac if normalized.
	if (rep_type == 1)
		frac_num += 1;

	printf("\tfraction:\t%.20f\n", frac_num); // FIX THIS 


	// Value in double
	V = pow(-1, signbit) * frac_num * pow(2, b_exp);
	printf("\tvalue:\t\t%.20f\n", V);

	// Value in scientific notation
	printf("\tvalue:\t\t%.20le\n", V);



	printf("\n");


	return;
}

