#include <stdio.h>
#include <stdlib.h>

#define MAX 100
#define OCTMAX 4
#define HEXMAX 2

int main()
{
	char input[MAX] = {'\n'};
	int dec[MAX] = {0};

	fgets(input, MAX, stdin);

	// Decimal conversion
	for (int i = 0; input[i] != '\n'; i++)
		dec[i] = (int)input[i]; // set the ascii values/decimals to each array

	printf("octal output\n");
	
	// Octal Output
	for (int i = 0; dec[i] != 0; i++)
	{
		printf("%.4o", dec[i]);
		printf(" ");
	}

	printf("\ndecimal output\n");

	// Decimal Output
	for (int i = 0; dec[i] != 0; i++)
		printf("%d ", dec[i]);

	printf("\nhex output\n");
	// Hex Output
	for (int i = 0; dec[i] != 0; i++)
	{
		printf("0x%x", dec[i]);
		printf(" ");
	}
	printf("\n");
	return 0;
}
