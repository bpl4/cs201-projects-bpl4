#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int base = 0;
	int userinput = 0;
	int i = 0;
	int oct[100] = {0};
	int dec[100] = {0};

	scanf("%d", &base);

	if (base == 8)
	{
		printf("octal input\n");
		scanf("%o", &userinput);
		while(userinput != 0 && i < 100)
		{
			oct[i] = userinput;
			i++;
			scanf("%o", &userinput);
		}
		for (int a = 0; a < 100; a++)
		{
			printf("%c", oct[a]);
		}
		printf("\n");
	}

	if (base == 10)
	{
		printf("decimal input\n");
		scanf("%d", &userinput);
		while(userinput != 0 && i < 100)
		{
			dec[i] = userinput;
			i++;
			scanf("%d", &userinput);
		}
		for (int a = 0; a < 100; a++)
		{
			printf("%c", dec[a]);
		}
		printf("\n");
	}

	if (base == 16)
	{
		printf("hex input\n");
		scanf("%x", &userinput);
		while(userinput != 0 && i < 100)
		{
			dec[i] = userinput;
			i++;
			scanf("%x", &userinput);
		}
		for (int a = 0; a < 100; a++)
		{
			printf("%c", dec[a]);
		}
		printf("\n");
	}

	return 0;
}
